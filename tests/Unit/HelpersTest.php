<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpersTest extends TestCase
{
    public function testTransformDateHelper()
    {
        $this->assertEquals('Dec%2004,%202017', transformDate('2017-12-04'));
    }
}
