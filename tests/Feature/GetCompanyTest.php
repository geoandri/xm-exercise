<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetCompanyTest extends TestCase
{
    public function testGetCompanyData()
    {
        $response = $this->json('GET', 'api/company/ABAX?start_date=2017-11-07&end_date=2017-11-09');

        $response
            ->assertStatus(200)
            ->assertSee('9-Nov-17')
            ->assertSee('8-Nov-17')
            ->assertSee('7-Nov-17');
    }
}