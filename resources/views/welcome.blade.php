<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>XM Exercise</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">            
            <div class="content">                
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading">Historical Quotes</div>

                                <div class="panel-body">
                                    <div id="form-errors"></div>
                                    <form id="quotesForm" class="form-horizontal" method="POST" action="{{ route('post_company') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('company_symbol') ? ' has-error' : '' }}">
                                            <label for="company_symbol" class="col-md-4 control-label">Company Symbol</label>

                                            <div class="col-md-6">
                                                <input id="company_symbol" type="text" class="form-control" name="company_symbol" value="{{ old('company_symbol') }}" required autofocus>

                                                @if ($errors->has('company_symbol'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('company_symbol') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>                                        

                                        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                            <label for="start_date" class="col-md-4 control-label">Start Date</label>

                                            <div class="col-md-6">
                                                <input id="start_date" type="text" class="form-control" name="start_date" required>

                                                @if ($errors->has('start_date'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('start_date') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                                            <label for="end_date" class="col-md-4 control-label">End Date</label>

                                            <div class="col-md-6">
                                                <input id="end_date" type="text" class="form-control" name="end_date" required>

                                                @if ($errors->has('end_date'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('end_date') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>                                        

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary" dusk="submit-button">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div id="quotesTable">
                            </div>
                            <div id="quotesChart" style="width: 100%; ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</html>
