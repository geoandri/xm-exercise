<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class visitMainPageTest extends DuskTestCase
{    
    public function testLaunchMainPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Historical Quotes');
        });
    }

    public function testSubmitForm()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('company_symbol', 'ABAX')
                    ->type('start_date', '2017-11-19')
                    ->type('end_date', '2017-11-23')
                    ->type('email', 'geoandri@gmail.com')
                    ->click('@submit-button')
                    ->waitFor('.table')
                    ->assertSee('Open')
                    ->assertSee('Close')
                    ->assertSee('22-Nov-17')
                    ->assertSee('21-Nov-17');
        });
    }

    public function testInvalidSymbolSubmit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('company_symbol', 'ABAXzzz')
                    ->type('start_date', '2017-11-19')
                    ->type('end_date', '2017-11-23')
                    ->type('email', 'geoandri@gmail.com')
                    ->click('@submit-button')
                    ->waitFor('.alert-danger')
                    ->assertSee('The selected company symbol is invalid.');
        });
    }

    public function testInvalidStartDateGreaterThanEndDateSubmit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('company_symbol', 'ABAX')
                    ->type('start_date', '2017-11-27')
                    ->type('end_date', '2017-11-23')
                    ->type('email', 'geoandri@gmail.com')
                    ->click('@submit-button')
                    ->waitFor('.alert-danger')
                    ->assertSee('The start date must be a date before end date.')
                    ->assertSee('The end date must be a date after start date.');
        });
    }

    public function testEmptySymbolSubmit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('start_date', '2017-11-19')
                    ->type('end_date', '2017-11-23')
                    ->type('email', 'geoandri@gmail.com')
                    ->click('@submit-button')
                    ->pause(5000)
                    ->assertDontSee('22-Nov-17');
        });
    }

    public function testEmptyEmailSubmit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('company_symbol', 'ABAX')
                    ->type('start_date', '2017-11-19')
                    ->type('end_date', '2017-11-23')
                    ->click('@submit-button')
                    ->pause(5000)
                    ->assertDontSee('22-Nov-17');
        });
    }

    public function testEmptyStartDateSubmit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('company_symbol', 'ABAX')
                    ->type('end_date', '2017-11-23')
                    ->type('email', 'geoandri@gmail.com')
                    ->click('@submit-button')
                    ->pause(5000)
                    ->assertDontSee('22-Nov-17');
        });
    }

    public function testEmptyEndDateSubmit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('company_symbol', 'ABAX')
                    ->type('start_date', '2017-11-19')
                    ->type('email', 'geoandri@gmail.com')
                    ->click('@submit-button')
                    ->pause(5000)
                    ->assertDontSee('22-Nov-17');
        });
    }

    
}
