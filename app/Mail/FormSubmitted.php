<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $start_date, $end_date)
    {
        $this->subject = $subject;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.submitted')
                    ->subject($this->subject)
                    ->with(['data' => [
                        'start_date' => $this->start_date,
                        'end_date' => $this->end_date,
                    ]]);
    }
}
