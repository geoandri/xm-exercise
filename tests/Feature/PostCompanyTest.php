<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Mail\FormSubmitted;
use Illuminate\Support\Facades\Mail;
use Carbon;

class PostCompanyTest extends TestCase
{
    public function testPostValidData()
    {
        $response = $this->json('POST', 'api/company', 
            ['company_symbol' => 'ABAX',
             'email' => 'gigigi@gmail.com',
             'start_date' => '2017-12-01',
             'end_date' => '2017-12-06'
         ]);

        $response->assertStatus(200);
    }

    public function testPostInvalidSymbolData()
    {
        $response = $this->json('POST', 'api/company', 
            ['company_symbol' => 'DADADADAD',
             'email' => 'gigigi@gmail.com',
             'start_date' => '2017-12-01',
             'end_date' => '2017-12-06'
         ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
            ]);
    }

    public function testPostInvalidMailData()
    {
        $response = $this->json('POST', 'api/company', 
            ['company_symbol' => 'ABAX',
             'email' => 'gigigigmail.com',
             'start_date' => '2017-12-01',
             'end_date' => '2017-12-06'
         ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
            ]);
    }

    public function testPostStarDateGreaterThanEndDate()
    {
        $response = $this->json('POST', 'api/company', 
            ['company_symbol' => 'ABAX',
             'email' => 'gigigi@gmail.com',
             'start_date' => '2017-12-01',
             'end_date' => '2017-11-06'
         ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
            ]);
    }

    public function testPostEndDateGreaterThanToday()
    {
        $response = $this->json('POST', 'api/company', 
            ['company_symbol' => 'ABAX',
             'email' => 'gigigi@gmail.com',
             'start_date' => '2017-12-01',
             'end_date' => Carbon\Carbon::now()->addDays(1)->format('Y-m-d')
         ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
            ]);
    }

    public function testMailSent()
    {
        Mail::fake();

        $email = 'gigigi@gmail.com';

        $response = $this->json('POST', 'api/company', 
            ['company_symbol' => 'ABAX',
             'email' => $email,
             'start_date' => '2017-12-01',
             'end_date' => '2017-12-06'
         ]);        

        Mail::AssertSent(FormSubmitted::class, function ($mail) use ($email) {
            return $mail->hasTo($email);
        });
    }
}
