<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_symbol' => 'required|exists:companies,symbol',
            'start_date' => 'required|date|before:end_date|before:tomorrow',
            'end_date' => 'required|date|after:start_date|before:tomorrow',
            'email' => 'required|email',
        ];
    }
}
