<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Input;
use App\Company;
use Mail;
use App\Mail\FormSubmitted;

class CompanyController extends Controller
{
    public function get_quotes($symbol)
    {   
        $data = apiCall($symbol, Input::get('start_date'), Input::get('end_date'));

        return response()->json($data);
    }

    public function post(CompanyRequest $request)
    {
        $company = Company::where('symbol', $request->input('company_symbol'))->first();

        Mail::to($request->input('email'))->send(new FormSubmitted($company->name, $request->input('start_date'), $request->input('end_date')));

        return response()->json(['success' => 'success'], 200);
    }
}
