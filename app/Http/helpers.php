<?php

function apiCall($symbol, $start_date, $end_date)
{
    $client = new \GuzzleHttp\Client();
    $api_url = 'https://finance.google.com/finance/historical?output=csv&q='.$symbol.'&startdate='.transformDate($start_date).'&enddate='.transformDate($end_date);

    $res = $client->request('GET', $api_url);
    $csv = $res->getBody()->getContents();

    $array = array_map('str_getcsv', explode(PHP_EOL, $csv));
    array_shift($array);
    array_pop($array);
    
    return $array;
}

function transformDate($date)
{
    return str_replace(" ", "%20", date('M d, Y', strtotime($date)));
}