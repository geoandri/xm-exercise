<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/company/{symbol}', 'CompanyController@get_quotes')->where('symbol', '[A-Z]+');
Route::post('/company', 'CompanyController@post')->name('post_company');