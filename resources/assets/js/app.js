$(document).ready(function(){
    $(function() {
        $( "#start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: new Date
        });
    });
    $(function() {
        $( "#end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: new Date
        });
    });
    $("#quotesForm").submit(function(event) {

        var formData = {
            'company_symbol': $('input[name=company_symbol]').val(),
            'email'         : $('input[name=email]').val(),
            'start_date'    : $('input[name=start_date]').val(),
            'end_date'      : $('input[name=end_date]').val(),
        };
        
        $.ajax({
            type        : 'POST',
            url         : 'api/company',
            data        : formData,
            dataType    : 'json',
            encode          : true,
            success: function(data) {
            $( '#form-errors' ).hide();
            getQuotes(formData['company_symbol'], formData['start_date'], formData['end_date']);
            },
            error: function(xhr) {
            var errors = xhr.responseJSON;
            var errorsHtml = '<div class="alert alert-danger"><ul>';
            $.each( errors.errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>';
            });
            errorsHtml += '</ul></div>';
            
            $( '#form-errors' ).html( errorsHtml );
             }
        })
        event.preventDefault();
    });
    function getQuotes(company_symbol, start_date, end_date)
    {
        $.ajax({
            type        : 'GET',
            url         : 'api/company/' + company_symbol,
            data        : {
                start_date: start_date, 
                end_date: end_date
            },
            dataType    : 'json',
            encode          : true,
            success: function(data) {
                var tableHtml = `<table class="table">
                                  <thead>
                                    <tr>
                                      <th scope="col">Date</th>
                                      <th scope="col">Open</th>
                                      <th scope="col">High</th>
                                      <th scope="col">Low</th>
                                      <th scope="col">Close</th>
                                      <th scope="col">Volume</th>
                                    </tr>
                                  </thead>
                                  <tbody>`;
                                  
                var tr = '';
                var dataArray = [];
                
                $.each(data, function(i, quote) {
                tr += '<tr>';
                tr += '<td class="text-left">' + quote[0] + '</td>';
                tr += '<td class="text-left">' + quote[1] + '</td>';
                tr += '<td class="text-left">' + quote[2] + '</td>';
                tr += '<td class="text-left">' + quote[3] + '</td>';
                tr += '<td class="text-left">' + quote[4] + '</td>';
                tr += '<td class="text-left">' + quote[5] + '</td>';
                tr += '</tr>';

                dataArray.push([quote[0], parseFloat(quote[1]), parseFloat(quote[4])]);
                });

                tableHtml += tr;
                tableHtml += '</tbody></table>';

                $( '#quotesTable' ).html( tableHtml );

                drawChart(dataArray.reverse());
            },
            error: function(xhr) {
             alert('Something failed. Sorry for this');
             }
        });
    }
    google.charts.load('current', {packages: ['corechart', 'line']});

    function drawChart(quotes_data) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Date');
        data.addColumn('number', 'Open price');
        data.addColumn('number', 'Close price');

        data.addRows(quotes_data);

        var options = {
          title: 'Quotes chart',
          legend: { position: 'bottom' },
          hAxis: { title: 'Date', format: 'Date' },
          colors: ['#a52714', '#097138'],
          crosshair: {
            color: '#000',
            trigger: 'selection'
          }
        };

        var chart = new google.visualization.LineChart(document.getElementById('quotesChart'));

        chart.draw(data, options);
    }
});